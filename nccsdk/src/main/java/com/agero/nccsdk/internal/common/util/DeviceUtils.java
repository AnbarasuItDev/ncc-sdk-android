package com.agero.nccsdk.internal.common.util;

import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;

import com.agero.nccsdk.internal.log.Timber;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.Locale;

import static com.agero.nccsdk.internal.common.util.StringUtils.capitalize;

/**
 * Created by james hermida on 8/14/17.
 */

public class DeviceUtils {

    public static String getDeviceId(Context context) {
        return Installation.getDeviceId(context);
    }

    public static String getOsVersion() {
        return Build.VERSION.RELEASE;
    }

    /**
     * Checks if Google Play Services is currently available and connected
     *
     * @param context Application context
     * @return true if Google Play Services is available, otherwise false
     */
    public static boolean isGooglePlayServicesAvailable(final Context context) {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int code = api.isGooglePlayServicesAvailable(context);
        return code == ConnectionResult.SUCCESS;
    }

    @SuppressWarnings({"MissingPermission"})
    public static boolean isConnectedToWiFi(final Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        boolean isWiFiConnected;

        // Log active network info
        StringBuilder networksLogBuilder = new StringBuilder();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = cm.getAllNetworks();
            if (networks != null) {
                int count = networks.length;
                for (int i = 0; i < count; i++) {
                    networksLogBuilder.append(String.format(Locale.getDefault(), "%d - %s\n", i, cm.getNetworkInfo(networks[i])));
                }
            }
        } else {
            NetworkInfo[] networkInfos = cm.getAllNetworkInfo();
            if (networkInfos != null) {
                int count = networkInfos.length;
                for (int i = 0; i < count; i++) {
                    networksLogBuilder.append(String.format(Locale.getDefault(), "%d - %s", i, networkInfos[i]));
                }
            }
        }
        Timber.v("%s", networksLogBuilder.toString().trim());

        // WifiInfo.getSSID() may be return null if there is no network currently connected
        if (wifiInfo.getSSID() == null) {
            Timber.w("wifiInfo.getSSID() returned null");
            isWiFiConnected = false;
        } else {
            String ssid = wifiInfo.getSSID().replace("\"", "");
            Timber.v("networkInfo.isConnected(): %s, ssid: %s, type: %s", networkInfo.isConnected(), ssid, networkInfo.getTypeName());
            isWiFiConnected = networkInfo.isConnected() && !ssid.equalsIgnoreCase("<unknown ssid>") && networkInfo.getTypeName().equals("WIFI");
        }
        return isWiFiConnected;
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    public static boolean isLocationSettingOnWithHighestAccuracy(final Context context) {
        // Android 4.4 KitKat introduced Location modes
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                int locationSetting = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
                return locationSetting == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY;
            } catch (Settings.SettingNotFoundException snfe) {
                Timber.w(snfe, "Location mode setting not available on device");
            }

        // Below Android 4.4 KitKit, GPS and network providers are available
        } else {
            final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            return manager != null
                    && manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                    && manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }

        return false;
    }
}
