package com.agero.nccsdk.internal.common.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.agero.nccsdk.internal.log.Timber;

/**
 * Created by james hermida on 2/9/18.
 */

public class AppUtils {

    public static String getAppVersion(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Timber.e(e, "Exception getting app version name");
        }

        return null;
    }

    public static String getAppPackage(Context context) {
        return context.getPackageName();
    }

}
