package com.agero.nccsdk.adt.detection.domain.geofence;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Intent;
import android.location.Location;
import android.text.TextUtils;

import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.R;
import com.agero.nccsdk.adt.detection.start.algo.GeofenceDrivingStartStrategy;
import com.agero.nccsdk.domain.data.NccGeofenceEventData;
import com.agero.nccsdk.internal.common.util.LocationUtils;
import com.agero.nccsdk.internal.log.Timber;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

import javax.inject.Inject;

/**
 * This class receives geofence transition events from Location Services, in the
 * form of an Intent containing the transition type and geofence id(s) that triggered
 * the event.
 */
public class GeofenceTransitionsIntentService extends IntentService {

    @Inject
    GeofenceManager geofenceManager;

    private GeofenceDrivingStartStrategy geofenceCollectionStartStrategy;
    
    /**
     * Sets an identifier for this class' background thread
     */
    public GeofenceTransitionsIntentService() {
        super(GeofenceTransitionsIntentService.class.getName());
        NccSdk.getComponent().inject(this);
    }

    /**
     * Handles incoming intents
     * @param intent The Intent sent by Location Services. This Intent is provided
     * to Location Services (inside a PendingIntent) when you call addGeofences()
     */
    @SuppressLint("StringFormatMatches")
    @Override
    protected void onHandleIntent(Intent intent) {
        geofenceCollectionStartStrategy = new GeofenceDrivingStartStrategy(getApplicationContext(), geofenceManager);

        LocationUtils.isDeviceUsingRequiredLocationSettings();

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent == null) {
            Timber.e("GeofencingEvent not included in intent");
        } else if (geofencingEvent.hasError()) {
            Timber.e("Geofence event error: %s", GeofenceErrorMessages.getErrorString(this, geofencingEvent.getErrorCode()));
        } else {
            logTransition(geofencingEvent);

            NccGeofenceEventData geofenceEvent = new NccGeofenceEventData(
                    System.currentTimeMillis(),
                    geofencingEvent.getTriggeringGeofences(),
                    geofencingEvent.getTriggeringLocation(),
                    geofencingEvent.getGeofenceTransition(),
                    geofencingEvent.hasError(),
                    geofencingEvent.getErrorCode()
            );

            if (geofenceEvent.getGeofenceTransition() == Geofence.GEOFENCE_TRANSITION_ENTER) {
                // TODO stop session?
                // This can help preserve battery if the device is constantly breaking and entering a geofence
                // while the user is walking, or if the device picks up locations outside the geofence.
            } else if (geofenceEvent.getGeofenceTransition() == Geofence.GEOFENCE_TRANSITION_EXIT) {
                geofenceCollectionStartStrategy.evaluate(geofenceEvent);
            } else {
                Timber.e(getString(R.string.geofence_transition_invalid_type, geofenceEvent.getGeofenceTransition()));
            }
        }
    }

    /**
     * Maps geofence transition types to their human-readable equivalents.
     *
     * @param transitionType    A transition type constant defined in Geofence
     * @return                  A String indicating the type of transition
     */
    private String getTransitionString(int transitionType) {
        switch (transitionType) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return "Entered";
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return "Exited";
            case Geofence.GEOFENCE_TRANSITION_DWELL:
                return "Dwell";
            default:
                return "Unknown transition";
        }
    }

    private void logTransition(GeofencingEvent geofencingEvent) {
        // Log the transition type and ids
        List<Geofence> geofences = geofencingEvent.getTriggeringGeofences();
        String[] geofenceIds = new String[geofences.size()];
        for (int index = 0; index < geofences.size(); index++) {
            geofenceIds[index] = geofences.get(index).getRequestId();
        }
        String ids = TextUtils.join(",", geofenceIds);
        String transitionType = getTransitionString(geofencingEvent.getGeofenceTransition());
        Timber.v(getString(R.string.geofence_transition_notification_title, transitionType, ids));

        // Log trigger location details
        Location triggerLocation = geofencingEvent.getTriggeringLocation();
        if (triggerLocation != null) {
            Timber.v("lat: %f, lng: %f, accuracy: %f, speed: %f",
                    triggerLocation.getLatitude(),
                    triggerLocation.getLongitude(),
                    triggerLocation.getAccuracy(),
                    triggerLocation.getSpeed()
            );
        } else {
            Timber.w("Trigger location is null");
        }
    }
}
