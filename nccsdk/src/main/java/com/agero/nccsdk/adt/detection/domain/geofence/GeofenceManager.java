package com.agero.nccsdk.adt.detection.domain.geofence;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.adt.detection.domain.geofence.model.NccGeofence;
import com.agero.nccsdk.domain.SensorManager;
import com.agero.nccsdk.domain.config.NccGeofenceConfig;
import com.agero.nccsdk.domain.config.NccLocationConfig;
import com.agero.nccsdk.domain.data.NccLocationData;
import com.agero.nccsdk.domain.data.NccSensorData;
import com.agero.nccsdk.domain.sensor.NccAbstractSensor;
import com.agero.nccsdk.internal.common.util.StringUtils;
import com.agero.nccsdk.internal.data.preferences.NccSharedPrefs;
import com.agero.nccsdk.internal.log.Timber;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.agero.nccsdk.internal.common.util.DeviceUtils.isGooglePlayServicesAvailable;

/**
 * This class is intended to be used for the existence of only one geofence at all times,
 * but can be easily updated for more.
 *
 */
@Singleton
public class GeofenceManager extends NccAbstractSensor {
    private final String GEOFENCE_ID_DEFAULT = "1";

    private final List<Geofence> googleGeofences;
    private PendingIntent geofencePendingIntent = null;

    private final Executor executor;
    private final NccSharedPrefs sharedPrefs;
    private final SensorManager sensorManager;
    private final Gson gson;

    private NccGeofence nccGeofence;

    private final GeofencingClient geofencingClient;
    private final OnCompleteListener<Void> addGeofenceCompleteListener;
    private final OnCompleteListener<Void> removeGeofenceCompleteListener;

    @Inject
    public GeofenceManager(Context context, GeofencingClient geofencingClient, NccSharedPrefs sharedPrefs, SensorManager sensorManager, Executor executor) {
        super(context, NccSensorType.GEOFENCE, new NccGeofenceConfig());

        this.executor = executor;
        this.geofencingClient = geofencingClient;
        this.sharedPrefs = sharedPrefs;
        this.sensorManager = sensorManager;
        
        googleGeofences = new ArrayList<>();
        gson = new Gson();

        addGeofenceCompleteListener = task -> {
            if (task.isSuccessful()) {
                // Store this flat version in SharedPreferences
                if (nccGeofence == null) {
                    Timber.e("onResult success called with TYPE_ADD_GEOFENCE but nccGeofence is null");
                } else {
                    // FIXME when a geofence is first added, an enter event can occur immediately, in which case this runnable may have not been executed (affects GeofenceTransitionsIntentService.logTransition())
                    this.executor.execute(() -> {
                        setGeofence(GEOFENCE_ID_DEFAULT, nccGeofence);
                        Timber.d("Geofence added");
                    });
                }
            } else {
                if (nccGeofence == null) {
                    Timber.e("Failed to create geofence");
                } else {
                    Timber.e("Failed to create geofence: " + nccGeofence.getLatitude() + "," + nccGeofence.getLongitude());
                }
            }
        };
        removeGeofenceCompleteListener = task -> {
            if (task.isSuccessful()) {
                this.executor.execute(() -> clearGeofence(GEOFENCE_ID_DEFAULT));
                googleGeofences.clear(); // remove if class is updated for multiple geofences
                Timber.d("Geofence removed");
            } else {
                Timber.e("Failed to remove geofence");
            }
        };
    }

    @Override
    public void startStreaming() {
        // DO NOTHING
    }

    @Override
    public void stopStreaming() {
        // DO NOTHING
    }

    /**
     * Create a geofence at the current location
     */
    public void createGeofenceAtCurrentLocation() {
        if (isGooglePlayServicesAvailable(applicationContext)) {
            subscribeLocationListener();
        } else {
            Timber.w("Google Play services not available. Unable to create geofence.");
        }
    }

    private void subscribeLocationListener() {
        Timber.v("Requesting location updates to create a geofence at the current location.");
        final NccGeofenceConfig config = (NccGeofenceConfig) this.config;
        NccSensorListener locationListener = new NccSensorListener() {
            @Override
            public void onDataChanged(NccSensorType sensorType, NccSensorData data) {
                NccLocationData location = (NccLocationData) data;
                String msg = "Updated Location: " +
                        Double.toString(location.getLatitude()) + ", " +
                        Double.toString(location.getLongitude());
                float accuracy = location.getAccuracy();
                if (accuracy < config.getRadius()) {
                    Timber.v(msg + "; Requesting geofence creation");
                    createGeofence(location.getLatitude(), location.getLongitude());

                    unsubscribeLocationListener(this);
                    // stop
                } else {
                    Timber.v("Minimum desired accuracy: " + config.getRadius() + "; Location ignored. Accuracy: " + accuracy);
                }
            }
        };

        try {
            sensorManager.subscribeSensorListener(NccSensorType.LOCATION, locationListener, new NccLocationConfig());
        } catch (NccException ne) {
            Timber.e(ne, "Exception subscribing to %s", NccSensorType.LOCATION.getName());
        }
    }

    private void unsubscribeLocationListener(NccSensorListener listener) {
        try {
            sensorManager.unsubscribeSensorListener(NccSensorType.LOCATION, listener);
        } catch (NccException ne) {
            Timber.e(ne, "Exception unsubscribing to %s", NccSensorType.LOCATION.getName());
        }
    }

    /**
     * Create a geofence at the specified geolocation
     * @param lat Latitude coordinate of the geofence
     * @param lng Longitude coordinate of the geofence
     * @return boolean true if geofence creation succeeded, otherwise false
     */
    @SuppressWarnings({"MissingPermission"})
    private void createGeofence(double lat, double lng) {
        if (isGooglePlayServicesAvailable(applicationContext)) {
            // Create a Google's geofence and add it to the list
            final NccGeofenceConfig config = (NccGeofenceConfig) this.config;
            Geofence googleGeofence = new Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId(GEOFENCE_ID_DEFAULT)
                    // Set the circular region of this geofence.
                    .setCircularRegion(
                            lat,
                            lng,
                            config.getRadius()
                    )
                    // Set the expiration duration of the geofence. This geofence gets automatically
                    // removed after this period of time.
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    // Set the transition types of interest. Alerts are only generated for these
                    // transition. We track entry and exit transitions in this sample.
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                            Geofence.GEOFENCE_TRANSITION_EXIT)
                    // Create the geofence.
                    .build();
            googleGeofences.add(googleGeofence);

            // Create a NccGeofence which will be persisted in shared preferences
            nccGeofence = new NccGeofence(
                    GEOFENCE_ID_DEFAULT,
                    lat,
                    lng,
                    config.getRadius(),
                    Geofence.NEVER_EXPIRE,
                    Geofence.GEOFENCE_TRANSITION_ENTER |
                            Geofence.GEOFENCE_TRANSITION_EXIT);
            Timber.v("NccGeofence instantiated and initialized");

            geofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                    .addOnCompleteListener(addGeofenceCompleteListener);
        } else {
            Timber.w("Google Play services not available. Unable to create geofence.");
        }
    }

    /**
     * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
     * Also specifies how the geofence notifications are initially triggered.
     */
    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
        // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
        // is already inside that geofence.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);

        // Add the geofences to be monitored by geofencing service.
        builder.addGeofences(googleGeofences);

        // Return a GeofencingRequest.
        return builder.build();
    }

    /**
     * Gets a PendingIntent to send with the request to add or remove Geofences. Location Services
     * issues the Intent inside this PendingIntent whenever a geofence transition occurs for the
     * current list of geofences.
     *
     * @return A PendingIntent for the IntentService that handles geofence transitions.
     */
    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (geofencePendingIntent == null) {
            Intent intent = new Intent(applicationContext, GeofenceTransitionsIntentService.class);
            // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
            // addGeofences() and removeGeofences().
            geofencePendingIntent = PendingIntent.getService(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
        
        return geofencePendingIntent;
    }

    public NccGeofence getGeofence() {
        return getGeofence(GEOFENCE_ID_DEFAULT);
    }

    /**
     * Returns a stored geofence by its id, or returns {@code null}
     * if it's not found.
     *
     * @param id The ID of a stored geofence
     * @return A geofence defined by its center and radius. See
     * {@link NccGeofence}
     */
    private NccGeofence getGeofence(String id) {
        String geofenceString = sharedPrefs.getString(id);
        if (StringUtils.isNullOrEmpty(geofenceString)) {
            return null;
        } else {
            return gson.fromJson(geofenceString, NccGeofence.class);
        }
    }

    /**
     * Save a geofence.
     *
     * @param nccGeofence The {@link NccGeofence} containing the
     *                       values you want to save in SharedPreferences
     */
    private void setGeofence(String id, NccGeofence nccGeofence) {
        if (nccGeofence == null) {
            Timber.w("setGeofence called with null nccGeofence. No geofence being written to SharedPreferences.");
        } else {
            sharedPrefs.put(id, gson.toJson(nccGeofence));
        }
    }

    private void clearGeofence(String id) {
        sharedPrefs.remove(id);
    }

    /**
     * Delete the current geofence
     */
    public void deleteGeofence() {
        if (isGooglePlayServicesAvailable(applicationContext)) {
            geofencingClient.removeGeofences(getGeofencePendingIntent())
                    .addOnCompleteListener(removeGeofenceCompleteListener);
        } else {
            Timber.w("Google Play services not available. Unable to delete geofence.");
        }
    }

    public boolean geofenceExists() {
        return getGeofence(GEOFENCE_ID_DEFAULT) != null;
    }

    public LatLng getGeofenceLatLng() {
        LatLng geolocation = null;
        if (geofenceExists()) {
            NccGeofence g = getGeofence(GEOFENCE_ID_DEFAULT);
            if (g != null) {
                geolocation = new LatLng(g.getLatitude(), g.getLongitude());
            }
        }
        return geolocation;
    }
}
