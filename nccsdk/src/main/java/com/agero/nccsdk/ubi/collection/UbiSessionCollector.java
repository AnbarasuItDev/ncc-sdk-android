package com.agero.nccsdk.ubi.collection;

import android.content.Context;
import android.support.v4.util.ArrayMap;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccExceptionErrorCode;
import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.SensorManager;
import com.agero.nccsdk.internal.common.io.FileWriterWrapper;
import com.agero.nccsdk.internal.data.DataManager;
import com.agero.nccsdk.internal.di.qualifier.UbiDataPath;
import com.agero.nccsdk.internal.log.Timber;
import com.agero.nccsdk.ubi.collection.io.Headers;
import com.agero.nccsdk.ubi.collection.io.NccBufferedFileWriterWrapper;
import com.agero.nccsdk.ubi.collection.io.NccFileWriterWrapper;
import com.agero.nccsdk.ubi.collection.mapper.NccAccelerometerMapper;
import com.agero.nccsdk.ubi.collection.mapper.NccBarometerMapper;
import com.agero.nccsdk.ubi.collection.mapper.NccBatteryMapper;
import com.agero.nccsdk.ubi.collection.mapper.NccGyroscopeMapper;
import com.agero.nccsdk.ubi.collection.mapper.NccGyroscopeUncalibratedMapper;
import com.agero.nccsdk.ubi.collection.mapper.NccLinearAccelerometerMapper;
import com.agero.nccsdk.ubi.collection.mapper.NccLocationMapper;
import com.agero.nccsdk.ubi.collection.mapper.NccMagneticFieldMapper;
import com.agero.nccsdk.ubi.collection.mapper.NccMagneticFieldUncalibratedMapper;
import com.agero.nccsdk.ubi.collection.mapper.NccMotionActivityMapper;
import com.agero.nccsdk.ubi.collection.mapper.NccRotationVectorMapper;
import com.agero.nccsdk.ubi.collection.mapper.SensorDataMapper;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by james hermida on 11/29/17.
 */

public class UbiSessionCollector extends AbstractSensorCollector {

    private final String TAG = UbiSessionCollector.class.getSimpleName();

    @Inject
    @UbiDataPath
    File dataPath;

    @Inject
    DataManager dataManager;

    @Inject
    SensorManager sensorManager;

    private final String collectionId;

    private final NccSensorType[] sensorTypes;
    private Map<NccSensorType, NccSensorListener> sensorListeners;

    private File sessionDataPathFile;
    private Headers headers;

    public UbiSessionCollector(Context applicationContext, String collectionId, NccSensorType[] sensorTypes) {
        super(applicationContext);
        this.collectionId = collectionId;
        this.sensorTypes = sensorTypes;
    }

    @Override
    public void start() {
        if (!isStarted()) {
            NccSdk.getComponent().inject(this);

            sessionDataPathFile = new File(dataPath + File.separator + collectionId);
            sessionDataPathFile.mkdirs();

            startSensorCollections(sensorTypes);
            isStarted = true;
        }
    }

    @Override
    public void stop() {
        if (isStarted()) {
            stopSensorCollections();
            isStarted = false;
        }
    }

    private void startSensorCollections(NccSensorType[] sensorTypes) {
        sensorListeners = new ArrayMap<>();
        for (NccSensorType sensorType : sensorTypes) {
            try {
                if (sensorManager.isSensorAvailable(sensorType)) {
                    NccSensorListener sensorListener;
                    try {
                        sensorListener = getSensorListener(sensorType);
                        sensorManager.subscribeSensorListener(
                                sensorType,
                                sensorListener,
                                dataManager.getConfig(sensorType)
                        );
                        sensorListeners.put(sensorType, sensorListener);
                    } catch (NccException | IOException e) {
                        Timber.e(e, "Exception subscribing to sensor %s", sensorType.getName());
                    }
                } else {
                    Timber.w("Sensor unavailable for UBI: %s", sensorType.getName());
                }
            } catch (NccException ne) {
                Timber.e(ne, "Exception checking sensor availability for sensor %s", sensorType.getName());
            }
        }
    }

    private NccSensorListener getSensorListener(NccSensorType sensorType) throws NccException, IOException {
        FileWriterWrapper fileWriterWrapper = getFileWriter(sensorType);
        SensorDataMapper sensorDataMapper = getSensorDataMapper(sensorType);
        return new FileSensorListener(fileWriterWrapper, sensorDataMapper);
    }

    private FileWriterWrapper getFileWriter(NccSensorType sensorType) throws IOException {
        // Init file headers
        if (headers == null) {
            headers = new Headers();
            headers.addDefaultHeaders(dataManager.getUserId());
        }

        switch (sensorType) {
            case LOCATION:
            case MOTION_ACTIVITY:
            case BATTERY:
                // These sensors do not come in a fast rate, so write the data to the file as it comes in
                return new NccFileWriterWrapper(getOutputFile(sessionDataPathFile.toString(), sensorType), headers);
            default:
                return new NccBufferedFileWriterWrapper(getOutputFile(sessionDataPathFile.toString(), sensorType), headers);
        }
    }

    private File getOutputFile(String sessionDir, NccSensorType sensorType) {
        // Init file based on sensor type
        File file = new File(sessionDir + File.separator + sensorType.getShortIdentifier());
        Timber.v(sensorType.getName() + " data file: " + file.getAbsolutePath());
        return file;
    }

    private SensorDataMapper getSensorDataMapper(NccSensorType sensorType) throws NccException {
        switch (sensorType) {
            case LOCATION:
                return new NccLocationMapper();
            case MOTION_ACTIVITY:
                return new NccMotionActivityMapper();

            // Native sensors
            case ACCELEROMETER:
                return new NccAccelerometerMapper();
            case LINEAR_ACCELEROMETER:
                return new NccLinearAccelerometerMapper();
            case GYROSCOPE:
                return new NccGyroscopeMapper();
            case GYROSCOPE_UNCALIBRATED:
                return new NccGyroscopeUncalibratedMapper();
            case MAGNETIC_FIELD:
                return new NccMagneticFieldMapper();
            case MAGNETIC_FIELD_UNCALIBRATED:
                return new NccMagneticFieldUncalibratedMapper();
            case ROTATION_VECTOR:
                return new NccRotationVectorMapper();
            case BAROMETER:
                return new NccBarometerMapper();

            // Other
            case BATTERY:
                return new NccBatteryMapper();
            default:
                throw new NccException(TAG, "Unknown sensor type '" + sensorType.getName() + "'", NccExceptionErrorCode.UNKNOWN_ERROR);
        }
    }

    private void stopSensorCollections() {
        unsubscribeCollectionSensors();
        closeCollectionDataWriters();
    }

    private void unsubscribeCollectionSensors() {
        if (sensorListeners == null) {
            Timber.e("Cannot unsubscribe sensors. sensorListeners is null");
        } else {
            for (Map.Entry<NccSensorType, NccSensorListener> entry : sensorListeners.entrySet()) {
                try {
                    sensorManager.unsubscribeSensorListener(entry.getKey(), entry.getValue());
                } catch (NccException ne) {
                    Timber.e(ne, "Exception unsubscribing to sensor %s", entry.getKey().getName());
                }
            }
        }
    }

    private void closeCollectionDataWriters() {
        if (sensorListeners == null) {
            Timber.e("Cannot close writers. sensorListeners is null");
        } else {
            for (Map.Entry<NccSensorType, NccSensorListener> entry : sensorListeners.entrySet()) {
                FileSensorListener sensorListener = (FileSensorListener) entry.getValue();
                FileWriterWrapper fileWriterWrapper = sensorListener.getSensorWriter();
                if (fileWriterWrapper == null) {
                    Timber.w("Attempt to close a null file writer for sensor %s", entry.getKey().getName());
                } else {
                    try {
                        sensorListener.getSensorWriter().close();
                    } catch (IOException ioe) {
                        Timber.e(ioe, "Exception closing file writer for file %s", fileWriterWrapper.getFile().getAbsolutePath());
                    }
                }
            }
        }
    }
}
