package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccLinearAccelerometerData;
import com.agero.nccsdk.domain.data.NccSensorData;

/**
 * Created by james hermida on 10/27/17.
 */

public class NccLinearAccelerometerMapper extends AbstractSensorDataMapper {

    @Override
    public String map(NccSensorData data) {
        NccLinearAccelerometerData lad = (NccLinearAccelerometerData) data;
        clearStringBuilder();
        sb.append("A,");                                // Code
        sb.append(lad.getX()); sb.append(",");          // userAx
        sb.append(lad.getY()); sb.append(",");          // userAy
        sb.append(lad.getZ()); sb.append(",");          // userAz
        sb.append("NA,NA,NA,NA,NA,NA,");
        sb.append(lad.getTimestamp()); sb.append(",");  // UTCTime
        sb.append(getReadableTimestamp(lad.getTimestamp())); // Timestamp
        return sb.toString();
    }
}
