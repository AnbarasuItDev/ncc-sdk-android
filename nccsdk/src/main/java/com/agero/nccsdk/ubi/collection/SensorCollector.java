package com.agero.nccsdk.ubi.collection;

/**
 * Created by james hermida on 11/29/17.
 */

public interface SensorCollector {

    void start();
    void stop();
    boolean isStarted();

}
