package com.agero.nccsdk.ubi;

import android.content.Context;

import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;
import com.agero.nccsdk.internal.log.Timber;
import com.agero.nccsdk.ubi.collection.SensorCollector;
import com.agero.nccsdk.ubi.collection.UbiSessionCollector;

import java.util.UUID;

/**
 * Created by james hermida on 9/6/17.
 */

public class UbiCollectionState extends UbiAbstractState {

    UbiCollectionState(Context context) {
        super(context);
    }

    private SensorCollector ubiSessionCollector;

    private String sessionId;

    @Override
    public void onEnter() {
        Timber.d("Entered UbiCollectionState");
        sessionId = UUID.randomUUID().toString();
        Timber.d("Session id: " + sessionId);

        ubiSessionCollector = new UbiSessionCollector(
                applicationContext,
                sessionId,
                new NccSensorType[]{
                        NccSensorType.LOCATION,
                        NccSensorType.MOTION_ACTIVITY,
                        NccSensorType.ACCELEROMETER,
                        NccSensorType.LINEAR_ACCELEROMETER,
                        NccSensorType.GYROSCOPE,
                        NccSensorType.GYROSCOPE_UNCALIBRATED,
                        NccSensorType.MAGNETIC_FIELD,
                        NccSensorType.MAGNETIC_FIELD_UNCALIBRATED,
                        NccSensorType.ROTATION_VECTOR,
                        NccSensorType.BAROMETER,
                        NccSensorType.BATTERY
                });

        ubiSessionCollector.start();
    }

    @Override
    public void onExit() {
        Timber.d("Exited UbiCollectionState");
        ubiSessionCollector.stop();
    }

    @Override
    public void startCollecting(StateMachine<UbiState> machine) {
        // DO NOTHING
    }

    @Override
    public void stopCollecting(StateMachine<UbiState> machine) {
        machine.setState(new UbiInactiveState(applicationContext));
    }

    String getSessionId() {
        return sessionId;
    }
}
