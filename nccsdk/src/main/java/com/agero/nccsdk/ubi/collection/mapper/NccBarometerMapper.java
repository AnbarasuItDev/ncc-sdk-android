package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccBarometerData;
import com.agero.nccsdk.domain.data.NccSensorData;

/**
 * Created by james hermida on 10/27/17.
 */

public class NccBarometerMapper extends AbstractSensorDataMapper {

    static final float HPA_TO_PASCAL_MULTIPLIER = 100.0f;

    @Override
    public String map(NccSensorData data) {
        NccBarometerData bd = (NccBarometerData) data;
        float pascalConversion = bd.getPressure() * HPA_TO_PASCAL_MULTIPLIER;
        clearStringBuilder();
        sb.append("P,");                                // Code
        sb.append(pascalConversion); sb.append(",");    // hPa
        sb.append("NA,NA,NA,NA,NA,NA,NA,NA,");          // Ar (not available on Android)
        sb.append(bd.getTimestamp()); sb.append(",");   // UTCTime
        sb.append(getReadableTimestamp(bd.getTimestamp())); // Timestamp
        return sb.toString();
    }
}
