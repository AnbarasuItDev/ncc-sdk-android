package com.agero.nccsdk.domain.model;

public class NccBluetoothClass {

    private boolean isA2dp;
    private boolean isHpf;

    public NccBluetoothClass(boolean isA2dp, boolean isHpf) {
        this.isA2dp = isA2dp;
        this.isHpf = isHpf;
    }

    public boolean isA2dp() {
        return isA2dp;
    }

    public boolean isHpf() {
        return isHpf;
    }

    @Override
    public String toString() {
        return "NccBluetoothClass{" +
                "isA2dp=" + isA2dp +
                ", isHpf=" + isHpf +
                '}';
    }
}
