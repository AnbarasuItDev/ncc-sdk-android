package com.agero.nccsdk.domain.data;

import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;

/**
 * Created by james hermida on 11/14/17.
 */

public class NccLocationSettingsData extends NccAbstractSensorData {

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public enum Mode {

        UNAVAILABLE(-1, "Unavailable"),
        OFF(Settings.Secure.LOCATION_MODE_OFF, "Off"),
        SENSORS_ONLY(Settings.Secure.LOCATION_MODE_SENSORS_ONLY, "Sensors Only"),
        BATTERY_SAVING(Settings.Secure.LOCATION_MODE_BATTERY_SAVING, "Battery Saving"),
        HIGH_ACCURACY(Settings.Secure.LOCATION_MODE_HIGH_ACCURACY, "High Accuracy");

        private final int id;
        private final String name;

        Mode(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "Mode{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

    // defaults
    private Mode mode = Mode.UNAVAILABLE;
    private Provider provider = null;

    public NccLocationSettingsData(long timestamp, Mode mode) {
        super(timestamp);
        this.mode = mode;
    }

    public NccLocationSettingsData(long timestamp, Provider provider) {
        super(timestamp);
        this.provider = provider;
    }

    public Mode getMode() {
        return mode;
    }

    public Provider getProvider() {
        return provider;
    }

    /**
     * Providers are used below Android 4.4 KitKat
     */
    public static class Provider {
        private boolean isGpsEnabled = false;
        private boolean isNetworkEnabled = false;

        public Provider(boolean isGpsEnabled, boolean isNetworkEnabled) {
            this.isGpsEnabled = isGpsEnabled;
            this.isNetworkEnabled = isNetworkEnabled;
        }

        public boolean isGpsEnabled() {
            return isGpsEnabled;
        }

        public boolean isNetworkEnabled() {
            return isNetworkEnabled;
        }

        @Override
        public String toString() {
            return "Provider{" +
                    "isGpsEnabled=" + isGpsEnabled +
                    ", isNetworkEnabled=" + isNetworkEnabled +
                    '}';
        }
    }

    @Override
    public String toString() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return "NccLocationSettingsData{" +
                    "mode=" + mode.toString() +
                    ", timestamp=" + timestamp +
                    '}';
        } else {
            return "NccLocationSettingsData{" +
                    "provider=" + provider.toString() +
                    ", timestamp=" + timestamp +
                    '}';
        }
    }
}
